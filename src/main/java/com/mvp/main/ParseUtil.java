package com.mvp.main;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ParseUtil {

	private final static String HOURLY_FORECASTS = "hourlyForecasts";
	private final static String FORECAST_LOCACTION = "forecastLocation";
	private final static String FORECAST = "forecast";
	private final static SimpleDateFormat SDF = new SimpleDateFormat("MMddyyyy");

	public static void parseWeatherResponse(String response) {
		System.out.println("Parsing Weather response.");
		JSONParser parser = new JSONParser();

		try {
			JSONObject jsonObject = (JSONObject) parser.parse(response);
			JSONObject hourlyForecasts = (JSONObject) jsonObject.get(HOURLY_FORECASTS);
			JSONObject locationObj = (JSONObject) hourlyForecasts.get(FORECAST_LOCACTION);
			String city = locationObj.get("city").toString();

			List<Forecast> forecasts = new ArrayList<>();
			JSONArray forecastObj = (JSONArray) locationObj.get(FORECAST);
			for (int i = 0; i < forecastObj.size(); i++) {
				JSONObject obj = (JSONObject) forecastObj.get(i);
				Forecast forecast = WeatherUtil.setForecastData(obj);
				forecasts.add(forecast);
			}

			Date today = new Date();
			String tomorrow = SDF.format(WeatherUtil.getTomorrowsDate(today));
			List<Forecast> tomorrowsForecast = forecasts.stream().filter(f -> f.getLocalTime().contains(tomorrow))
					.collect(Collectors.toList());
			Forecast forecast = Collections.min(tomorrowsForecast, Comparator.comparing(f -> f.getTemperature()));
			String localTime = forecast.getLocalTime();
			System.out.println(WeatherUtil.getCoolestHour(localTime) + " is the coolest hour tomorrow '"
					+ forecast.getWeekday() + ", " + WeatherUtil.getDate(localTime) + "'\nin the city '" + city
					+ "' and the temperature will be: '" + forecast.getTemperature() + " F'.");

		} catch (ParseException e) {
			System.err.println("Exception while parsing json response collected from weather api: " + e.getMessage());
		}
	}
}
