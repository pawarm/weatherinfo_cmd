package com.mvp.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * This class is the main class, used to view tomorrow�s predicted temperatures
 * for a given zip-code in the United States.
 * 
 * @author pawarm
 *
 */
public class WeatherInfo {
	
	private static String URL = "https://weather.api.here.com/weather/1.0/report.json?";
	private static String app_id = "app_id=68yqvd4svKy6647IM77P";
	private static String app_code = "app_code=Mg8TwkZ17UEA0QfL5enFNw";
	private static String product = "product=forecast_hourly";
	private static String metric = "metric=false";

	public static void main(String[] args) throws IOException {
		String input;
		try {
			System.out.println("Welcome to the Weather Info App");
			System.out.print("Enter the 5 digit Zip code for the city in US: ");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

			while ((input = br.readLine()) != null && !input.equals("exit") && !input.equals("quit")) {
				try {
					int zipcode = Integer.parseInt(input);
					getWeatherDetails(zipcode);
				} catch (NumberFormatException e) {
					System.out.println("Please enter a valid zip code\n");
				}
				System.out.print(
						"\nWant to check temperature at another location,"
						+ "\nthen please Enter the 5 digit Zip code of the city in US or enter exit to quit: ");
			}
			System.out.println(".....Program Terminated.....");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void getWeatherDetails(int zipCode) {
		System.out.println("Getting Weather Details for the zip code '" + zipCode + "'.");
		try {
			String urlParams = app_id.concat("&").concat(app_code).concat("&").concat(product).concat("&").concat(metric);
			URL url = new URL(URL.concat(urlParams).concat("&zipcode=")
							+ zipCode);
			// Get the input stream through URL Connection
			URLConnection conn = url.openConnection();
			BufferedReader ar = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String response = ar.readLine();
			ParseUtil.parseWeatherResponse(response);
		} catch (IOException e) {
			System.out.println("City not found, please enter a valid Zip Code\n");
		}
	}
}