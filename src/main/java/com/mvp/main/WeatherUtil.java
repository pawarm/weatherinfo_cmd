package com.mvp.main;

import java.util.Calendar;
import java.util.Date;

import org.json.simple.JSONObject;

/**
 * Utility class to perform common operations
 * 
 * @author pawarm
 *
 */
public class WeatherUtil {
	
	/**
	 * This method is used to get Date
	 * 
	 * @param localTime
	 * @return
	 */
	public static String getDate(String localTime) {
		String month = localTime.substring(2, 4);
		String date = localTime.substring(4, 6);
		String year = localTime.substring(6, localTime.length());
		return (date + "-" + month + "-" + year);
	}

	/**
	 * This method is used to set Forecast data
	 * 
	 * @param obj
	 * @return
	 */
	public static Forecast setForecastData(JSONObject obj) {
		Forecast forecast = new Forecast();
		forecast.setTemperature(obj.get("temperature") == null ? "" : obj.get("temperature").toString());
		forecast.setWeekday(obj.get("weekday") == null ? "" : obj.get("weekday").toString());
		forecast.setLocalTime(obj.get("localTime") == null ? "" : obj.get("localTime").toString());
		return forecast;
	}

	/**
	 * This method is used to get tomorrows date
	 * 
	 * @param date
	 * @return
	 */
	public static Date getTomorrowsDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, 1);
		return cal.getTime();
	}

	/**
	 * This method is used to get the time of the day along with AM/PM
	 * 
	 * @param localTime
	 * @return String
	 */
	public static String getCoolestHour(String localTime) {

		String amPM = "AM";
		String time = localTime.substring(0, 2);
		if (Integer.parseInt(time) > 11) {
			if (time != "12")
				time = Integer.toString((Integer.parseInt(time) - 12));
			amPM = "pm";
		} else {
			if (time == "00")
				time = "12";
		}
		return (time + " " + amPM);
	}
}
